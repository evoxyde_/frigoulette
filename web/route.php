<?php

	//SESSION START
	session_start();
	require "configuration.php";
	$GLOBALS_INI= Configuration::getGlobalsINI();
	
	// Inclide autoloader of Twig
	require_once 'vendor/autoload.php';



	// Class dynamic
	if ((isset($_GET["page"])) && ($_GET["page"] != ""))	{
		$monPHP= $_GET["page"];
	}  else  {
		if ((isset($_POST["page"])) && ($_POST["page"] != ""))	{
			$monPHP= $_POST["page"];
		}  else  {
			$monPHP= "index";
		}
	}


	// list of classes authorized when you are NOT connected
	$list_class = array("accueil");
	if(!isset($_SESSION["id_utilisateur"]) ||(isset($_SESSION["id_utilisateur"])&& $_SESSION["id_utilisateur"]== "")){
		if(!in_array($monPHP, $list_class)){
			//$monPHP = "accueil";
		}		
	}

	// Test if classes exist
	if (!(file_exists($GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php"))) {
		$monPHP= "accueil";
	}

	$myClass= ucfirst($monPHP);

	require $GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_CLASS"] . $monPHP . ".php"; 

	$oMain= new $myClass();

	$page_to_load= "route.twig";
	//if AJAX WITH JSON
	if ((isset($oMain->VARS_HTML["bJSON"])) && ($oMain->VARS_HTML["bJSON"] == 1))	{
		$page_to_load= $monPHP . ".html";
	}

	try {
		// Définir le dossier ou on trouve les templates
		$loader = new \Twig\Loader\FilesystemLoader($GLOBALS_INI["PATH_HOME"] . $GLOBALS_INI["PATH_FILES"]);
		// initialiser l'environnement Twig
		$twig = new \Twig\Environment($loader);
		// charger le template
		$template = $twig->load($page_to_load);
		// Affecter les variables du template et appeller le rendu
		echo $template->render(array(
			'resultat' => $oMain->resultat,
			'VARS_HTML' => $oMain->VARS_HTML,
			'GLOBALS_INI' => $GLOBALS_INI, 
			'LANG' => $oMain->LANG,
			'monPHP' => $monPHP
		));
	} catch (Exception $e) {
		die ('ERROR: ' . $e->getMessage());
	}

	unset($oMain);
?> 