<?php
Class Database {
	
	private $_hDb;
	
	function __construct($host, $name, $login, $psw)	{
		// Connection to DB : SERVEUR / LOGIN / PASSWORD / NOM_BDD
		$this->_hDb= new PDO('mysql:host='.$host.';dbname='.$name.';charset=utf8', $login, $psw);
	}

	function __destruct()	{
		$this->_hDb= null;
	}
	
	public function getLastInsertId()	{
		return $this->_hDb->lastInsertId();
	}

	public function getSelectDatas($spathSQL, $data=array(), $bForJS=null)	{
		// content of SQL file
		$sql= file_get_contents($spathSQL);

		// replace variables @variable from sql by values of the same variables'name
		foreach ($data as $key => $value) {
			$value= str_replace("'", "__SIMPLEQUOT__", $value);
			$value= str_replace('"', '__DOUBLEQUOT__', $value);
			$value= str_replace(";", "__POINTVIRGULE__", $value);
			$value= str_replace('__APOSTROPHE__', '"', $value);
			$sql = str_replace('@'.$key, $value, $sql);
			//error_log("key = " . $key . " | " . "value= " . $value. " | " . "sql = " . $sql);
		}
		
		error_log("getSelectDatas = " . str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", " ", $sql) ) ) );
		// Execute la requete
		$results_db= $this->_hDb->prepare($sql);
		$results_db->execute();

		$resultat= [];
		while ($ligne = $results_db->fetch(PDO::FETCH_ASSOC)) {
			$new_ligne= [];
			foreach ($ligne as $key => $value) {
				//error_log("getSelectDatas DETAILS = " . $key . " => " . $value);
				if ((isset($bForJS)) && (($bForJS == 1) || ($bForJS == 2)))	{
					$value= str_replace("__SIMPLEQUOT__", "'", $value);
					$value= str_replace('__DOUBLEQUOT__', '\"', $value);
					$value= str_replace("__POINTVIRGULE__", ";", $value);
					/*if ($bForJS == 2)	{
						$value= utf8_encode($value);
					}*/
				}  else  {
					$value= str_replace("__SIMPLEQUOT__", "'", $value);
					$value= str_replace('__DOUBLEQUOT__', '"', $value);
					$value= str_replace("__POINTVIRGULE__", ";", $value);
				}
				$new_ligne[$key]= $value;
			}
			$resultat[]= $new_ligne;
		}
		return $resultat;
	}

	public function treatDatas($spathSQL, $data=array())	{
		// content of SQL file
		$sql= file_get_contents($spathSQL);

		// replace variables @variable from sql by values of the same variables'name
		foreach ($data as $key => $value) {
			$value= str_replace("'", "__SIMPLEQUOT__", $value);
			$value= str_replace('"', '__DOUBLEQUOT__', $value);
			$value= str_replace(";", "__POINTVIRGULE__", $value);
			$value= str_replace('__APOSTROPHE__', '"', $value);
			$sql= str_replace('@'.$key, $value, $sql);
			/*
			error_log('sql' .$sql);
			error_log('key '.$key);
			error_log('value '.$value);
			*/
		}

		$sql= str_replace('__MASSIVEPOINTVIRGULE__', ';', $sql);
		
		if (strstr($sql, ";"))	{
			$aOfSQL= explode(";", $sql);
			for($i=0; $i<count($aOfSQL); $i++)	{
				// Execute la requete
				if (strlen($aOfSQL[$i]) > 5)	{
					error_log("treatDatas = " . str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", " ", $aOfSQL[$i]) ) ) );
					$results_db= $this->_hDb->query($aOfSQL[$i]);
				}
			}
		}  else  {
			error_log("treatDatas = " . str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", " ", $sql) ) ) );
			// Execute la requete
			$results_db= $this->_hDb->query($sql);
		}

		return $results_db;
	}

}
	
?>
