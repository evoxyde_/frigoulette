<?php
class securite {

	public $VARS_HTML;
	
	function __construct()	{
		// put all variables $_POST et $_GET into the array $VARS_HTML
		$this->VARS_HTML= [];

		foreach($_POST as $key => $val)	{
			$this->VARS_HTML[$key]= $val;
		}

		foreach($_GET as $key => $val)	{
			$this->VARS_HTML[$key]= $val;
		}

		// default page : see route.php also
		if ( (!(isset($this->VARS_HTML["page"]))) || ($this->VARS_HTML["page"] == "") )	{
			$this->VARS_HTML["page"]= "accueil";
		}

		// default navigateur_code_langue
		if ( (!(isset($this->VARS_HTML["navigateur_code_langue"]))) || ($this->VARS_HTML["navigateur_code_langue"] == "") )	{
			$this->VARS_HTML["navigateur_code_langue"]= "fr";
		}
	}
}
?>
