<?php

// List of classes needed for this class
require_once "database.php";
require_once "security.php";
require_once "lang.php";


Abstract Class Initialize	{
	// Database instance object
	protected $oBdd;
	// All globals from INI
	protected $GLOBALS_INI;
	// Form Securite instance object
	private $oForms;
	public $VARS_HTML;
	// LANG from INI
	private $obj_lang;
	public $LANG;
	public $sInterface;

	public function __construct()	{
		// Instance of Config
		$this->GLOBALS_INI= Configuration::getGlobalsINI();

		// Instance of BDD
		$this->oBdd = new Database($this->GLOBALS_INI["DB_HOST"],
								   $this->GLOBALS_INI["DB_NAME"],
								   $this->GLOBALS_INI["DB_LOGIN"],
								   $this->GLOBALS_INI["DB_PSW"]);

		// Instance of Securite to have $this->VARS_HTML
		$this->oForms= new Securite();
		$this->VARS_HTML= $this->oForms->VARS_HTML;

		// Instance of Lang
		$this->obj_lang= new Lang();
		$this->LANG = $this->obj_lang->getTraductionsINI($this->GLOBALS_INI["PATH_HOME"] . $this->GLOBALS_INI["PATH_LANG"] . $this->VARS_HTML["navigateur_code_langue"] . ".ini", $this->VARS_HTML["page"]);
		$this->sInterface= $this->VARS_HTML["page"];
	}

	public function __destruct()	{
		// destroy Instance of Form
		unset($this->oForms);
		// disconnect of BDD
		// destroy Instance of BDD
		unset($this->oBdd);
		// destroy Instance of Lang
		unset($this->obj_lang);
	}

}

?>
