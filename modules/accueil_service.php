<?php

require_once "initialize.php";

/**
 * Class Accueil_service | file accueil_service.php
 *
 * In this class, we have methods for :
 *
 * Description of the class
 *
 * List of classes needed for this class
 *
 * require_once "initialize.php";
 *
 * @package FansnHumans Project
 * @subpackage Accueil_service
 * @author @FansnHumans Team - Prenom Nom Developper
 * @copyright  1920-2080 The FansnHumans Team 
 * @version v1.0
 */
class Accueil_service extends Initialize {

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * Call the parent constructor
     *
     * init variables resultat
     */
    public function __construct() {
        // Call Parent Constructor
        parent::__construct();

        // init variables resultat
        $this->resultat = [];
    }

    /**
     * Call the parent destructor
     */
    public function __destruct() {
        // Call Parent destructor
        parent::__destruct();
    }

}

?>
