<?php

require_once "accueil_service.php";

/**
 * Class Accueil | fichier accueil.php
 *
 * Description de la classe à renseigner.
 *
 * Cette classe nécessite l'utilisation de la classe :
 *
 * require_once "accueil_service.php";
 *
 * @package FansnHumans Project
 * @subpackage accueil_service
 * @author @FansnHumans Team - Prenom Nom Developper
 * @copyright  1920-2080 The FansnHumans Team
 * @version v1.0
 */
class Accueil {

	/**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

	/**
     * public $VARS_HTML is used to store all datas POSTED and GETTED
     * @var array
     */
    public $VARS_HTML;

	/**
     * public $LANG is used to store all datas localized in fr.ini, en.ini
     * @var array
     */
    public $LANG;

    /**
     * init variables resultat
     *
     * execute main function
     */
    public function __construct() {
        // init variables resultat
        $this->resultat = [];

        // execute main function
        $this->main();
    }

    /**
     *
     * Destroy service
     *
     */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
     * Get interface to gestion of Stagiaires
     */
    function main() {
		$objet_service = new Accueil_service();
		// Ici je fais mon appel $objet_service->ma_methode_qui_est_dans_le_service
		
		// Je passe mes paramètres pour y avoir accès dans mes pages HTML
		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;
		$this->LANG = $objet_service->LANG;
    }
	
}

?>

