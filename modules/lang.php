<?php 
	class Lang {
		public function getTraductionsINI($PATH_LANG_INI, $INTERFACE) {
			$LANG_INI= [];
			$FILE_INI= parse_ini_file($PATH_LANG_INI, true);
			if (isset($FILE_INI[$INTERFACE]))	{
				foreach($FILE_INI[$INTERFACE] as $key => $val)	{
					$val= str_replace('__LEFTBRACKET__', '(', $val);
					$val= str_replace('__RIGHTBRACKET__', ')', $val);
					$val= str_replace('__DOUBLEQUOT__', '"', $val);
					$val= str_replace("__SIMPLEQUOT__", "'", $val);
					$LANG_INI[$INTERFACE][$key]= $val;
				}
			}
			foreach($FILE_INI["all"] as $key => $val)	{
				$val= str_replace('__LEFTBRACKET__', '(', $val);
				$val= str_replace('__RIGHTBRACKET__', ')', $val);
				$val= str_replace('__DOUBLEQUOT__', '"', $val);
				$val= str_replace("__SIMPLEQUOT__", "'", $val);
				$LANG_INI["all"][$key]= $val;
			}
			return $LANG_INI;
		}
	}
?>
